# testApp

This application is a Node.js Express template to help you build your service application 

## Getting Started

These instructions will get you a template of the project up and running on your virtual machine for development and testing purposes.

### Prerequisites

To run the application you will need to have the following packages installed :

- node
- npm
- mariadb
- docker


### Installing

To install the project follow those steps :

- Clone the repository

- Go in the repository 
```bash
cd [repository_name]
```

- Install node modules (please check you are inside the repository before installing the modules)
```bash
npm install
```

### Run Application

To run the application :
- Check you are at the root of the repository
- Run the following command :
```bash
npm start
```

## Running the tests



### Break down into end to end tests



### And coding style tests



## Deployment



## Contributing

Please keep in mind that master branch need to stay stable release or be use only for minor bug fix


## Versioning


## Authors

* **Equipe Socle technique URSI SIGL2021** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Express js : https://expressjs.com/fr/
