var express = require('express');
var router = express.Router();

const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('app', 'root', 'test', {
  host: 'localhost',
  dialect: 'mariadb'
});

try {
  sequelize.authenticate();
  console.log('Connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

const User = sequelize.define('User', {
  // Model attributes are defined here
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  timestamps: false
  // Other model options go here
});

// `sequelize.define` also returns the model
console.log(User === sequelize.models.User); // true

User.sync();
console.log("The table for the User model was just (re)created!");



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/users', function(req, res, next) {
  User.findAll().then(function(users) {
    console.log(users);
    res.render('users', {title: 'Users', users: users});
  });
});

router.get('/api/user', function(req, res, next) {
  try {
    User.findAll().then(user => res.status(200).json(user));
  } catch (e) {
    res.status(404).json({'error': 'FindAll'});
  }
});


router.post('/api/user', function(req, res, next) {
  try {
    console.log(req.body);
    User.create(req.body).then(function (user) {
      res.status(200).json(user);
    });
  } catch (e) {
    res.status(404);
  }
});

module.exports = router;



